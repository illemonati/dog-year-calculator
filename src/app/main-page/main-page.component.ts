import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-main-page',
  templateUrl: 'main-page.component.html'
})
export class MainPageComponent implements OnInit {
  constructor() {}
  humanYears = 0;
  dogYears = 0;
  warningMessage = '';
  ngOnInit(): void {
    this.loadYears();
  }
  toDogYears(): void {
    if (this.humanYears === null) {
      this.warningMessage = 'Please make sure you entered a valid number for human years !';
      return;
    }
    this.warningMessage = '';
    this.dogYears = this.humanYears / 7;
  }
  toHumanYears(): void {
    if (this.dogYears === null) {
      this.warningMessage = 'Please make sure you entered a valid number for dog years !';
      return;
    }
    this.warningMessage = '';
    this.humanYears = this.dogYears * 7;
  }
  // the reason it saves both is to avoid having to redo calculations for really big numbers
  async saveYears(): Promise<void> {
    localStorage.setItem('humanYears', this.humanYears.toString());
    localStorage.setItem('dogYears', this.dogYears.toString());
  }
  async loadYears(): Promise<void> {
    this.dogYears = parseFloat(localStorage.getItem('dogYears'));
    this.humanYears = parseFloat(localStorage.getItem('humanYears'));
    console.log('load success');
  }
}
